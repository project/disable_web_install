<?php

namespace Drupal\disable_web_install\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class DisableWebInstallSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => ['onKernelRequest']];
  }

  /**
   * Kernel request event handler.
   */
  public function onKernelRequest(RequestEvent $event) {
    $uri = '';
    switch ($event->getRequest()->getPathInfo()) {
      case '/admin/reports/updates/install':
        $uri = 'admin/reports/updates';
        break;

      case '/admin/modules/install':
        $uri = 'admin/modules';
        break;

      case '/admin/theme/install':
        $uri = 'admin/appearance';
        break;
    }
    if ($uri !== '') {
      $event->setResponse(new RedirectResponse(\base_path() . $uri));
    }
  }

}
